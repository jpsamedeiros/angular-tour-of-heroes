import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})

export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];
  BIG_DESCRIPTION = 65;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes.slice(1, 5));
  }

  passIndexValue(index){
    this.checkHeroDescription(index);
  }

  checkHeroDescription(index){
    //returns true if description is too big, false if it's not
    let heroDescriptionLength = this.heroes[index].description.length;
    if (heroDescriptionLength > this.BIG_DESCRIPTION){
      return true;
    }else{
      return false;
    }
  }
}